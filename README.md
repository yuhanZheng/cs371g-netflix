# CS371g: Generic Programming Netflix Repo

* Name: Yuhan Zheng, Shuyan Li

* EID: yz24347, sl45644

* GitLab ID: yuhanZheng, shuyan_99

* Git SHA: 53ae3dc9d8a769d6b646766fb119080a4c333dc9

* GitLab Pipelines: https://gitlab.com/yuhanZheng/cs371g-netflix/-/pipelines

* Estimated completion time: 15 hrs

* Actual completion time: 20 hrs

* Comments: RunNetflix.ctd file borrowed from Piazza. Used 3 caches from the caches repo.