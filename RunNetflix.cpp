// --------------
// RunNetflix.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <ctime>    // clock

#include "Netflix.hpp"

// ----
// main
// ----

int main () {
    using namespace std;

    // const clock_t bc = clock();
    netflix_solve(cin, cout);
    // const clock_t ec = clock();
    // cout << "Time: " << ((ec-bc) * 1000.0 / CLOCKS_PER_SEC) << " milliseconds" << endl;

    return 0;
}
