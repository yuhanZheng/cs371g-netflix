// ---------
// Netflix.h
// ---------

#ifndef Netflix_h
#define Netflix_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator
#include <vector>   // vector
#include <utility>  // pair

using namespace std;

// ------------
// netflix_load
// ------------

/**
 * load different caches into the program
 * CorrectAnswers cache. Movie -> (CustomerID(int) -> True Rating(float));
 * MovieAverages cache. vector<float> MovieAverageRating [i - 1] = Average Rating for movie i;
 * CustomerAvgRating cache. CustomerID(int) -> Avg Rating of all ratings of this customer (float);
 * CustomerToDecadesAndRatings cache. CustomerID(int) -> (decadeIndex(short) -> Avg Rating that Customer has given for movies in that decade).
 */

int netflix_load_cache ();

// ------------
// netflix_read
// ------------

/**
 * read all movies and save them into the movies vector
 * read all customers and save them into the m_customers vector
 * m_customers[i] holds all customers for movies[i]
 * @param an istream
 * @return a pair of two vectors
 */
pair<vector<int>, vector<vector<int>>> netflix_read (istream& sin);

// ------------
// netflix_eval
// ------------

/**
 * @param an int(movie ID) and a vector of ints(customerIDs)
 * @return a vector of floats(predicted ratings), such that customers[i] corresponds to ratings[i]
 */
vector<float> netflix_eval (const int& movie, const vector<int>& customers);

// -------------
// netflix_print
// -------------

/**
 * print an int(movie ID), and a number of floats(predicted ratings)
 * @param an ostream, an int(movie ID), a vector of of floats(predicted ratings)
 */
void netflix_print (ostream& sout, const int& movie, const vector<float>& ratings);


// -------------
// netflix_rmse
// -------------

/**
 * print RMSE at the end
 * @param an ostream and a vector of floats
 */
void netflix_rmse (ostream& sout, vector<float> diff_sqr);

// -------------
// netflix_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void netflix_solve (istream&, ostream&);

#endif // Netflix_h
