// ---------------
// TestNetflix.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Netflix.hpp"

using namespace std;

// -----------
// TestNetflix
// -----------

// ----
// solve
// Have to be test first since the global diff_sqr vector would be altered
// in later tests, which would affect RMSE results.
// ----


TEST(NetflixFixture, solve0) {
    istringstream iss("10003:\n1515111\n");
    ostringstream oss;
    netflix_solve(iss, oss);
    ASSERT_EQ(oss.str(), "10003:\n3\nRMSE: 0");
}

TEST(NetflixFixture, solve1) {
    istringstream iss("10:\n1952305\n1531863\n");
    ostringstream oss;
    netflix_solve(iss, oss);
    ASSERT_EQ(oss.str(), "10:\n3.3\n3.1\nRMSE: 0.18");
}

// ----
// read
// ----

TEST(NetflixFixture, read0) {
    istringstream         iss("10:\n1952305\n250\n1000:\n2326571\n");
    pair<vector<int>, vector<vector<int>>> p = netflix_read(iss);
    vector<int> movies;
    vector<vector<int>> m_customers;
    tie(movies, m_customers) = p;
    ASSERT_EQ(movies[0], 10);
    ASSERT_EQ(movies[1], 1000);
    ASSERT_EQ(m_customers[0][0], 1952305);
    ASSERT_EQ(m_customers[0][1], 250);
    ASSERT_EQ(m_customers[1][0], 2326571);
}

TEST(NetflixFixture, read1) {
    istringstream         iss("2:\n1952\n");
    pair<vector<int>, vector<vector<int>>> p = netflix_read(iss);
    vector<int> movies;
    vector<vector<int>> m_customers;
    tie(movies, m_customers) = p;
    ASSERT_EQ(movies[0], 2);
    ASSERT_EQ(m_customers[0][0], 1952);
}

TEST(NetflixFixture, read2) {
    istringstream         iss("124:\n1952305\n250\n41241\n");
    pair<vector<int>, vector<vector<int>>> p = netflix_read(iss);
    vector<int> movies;
    vector<vector<int>> m_customers;
    tie(movies, m_customers) = p;
    ASSERT_EQ(movies[0], 124);
    ASSERT_EQ(m_customers[0][0], 1952305);
    ASSERT_EQ(m_customers[0][1], 250);
    ASSERT_EQ(m_customers[0][2], 41241);
}

// ----
// eval
// ----

TEST(NetflixFixture, eval0) {
    netflix_load_cache();
    int movie = 1;
    vector<int> customers({30878});
    vector<float> rating_predictions = netflix_eval (movie, customers);
    ASSERT_FLOAT_EQ(rating_predictions[0], 3.7);
}

TEST(NetflixFixture, eval1) {
    netflix_load_cache();
    int movie = 1;
    vector<int> customers({2647871});
    vector<float> rating_predictions = netflix_eval (movie, customers);
    ASSERT_FLOAT_EQ(rating_predictions[0], 3.6);
}

TEST(NetflixFixture, eval2) {
    netflix_load_cache();
    int movie = 1;
    vector<int> customers({1283744});
    vector<float> rating_predictions = netflix_eval (movie, customers);
    ASSERT_FLOAT_EQ(rating_predictions[0], 3.5);
}

// ----
// print
// ----

TEST(NetflixFixture, print0) {
    ostringstream oss;
    int movie = 2;
    vector<float> ratings = {2, 3, 4};
    netflix_print(oss, movie, ratings);
    ASSERT_EQ(oss.str(), "2:\n2\n3\n4\n");
}

TEST(NetflixFixture, print1) {
    ostringstream oss;
    int movie = 2332;
    vector<float> ratings = {2};
    netflix_print(oss, movie, ratings);
    ASSERT_EQ(oss.str(), "2332:\n2\n");
}

TEST(NetflixFixture, print2) {
    ostringstream oss;
    int movie = 213;
    vector<float> ratings = {2, 2, 2};
    netflix_print(oss, movie, ratings);
    ASSERT_EQ(oss.str(), "213:\n2\n2\n2\n");
}

// ----
// rmse
// ----

TEST(NetflixFixture, rmse0) {
    ostringstream oss;
    vector<float> diff_sqr = {1.0,1.0,1.0};
    netflix_rmse(oss, diff_sqr);
    ASSERT_EQ(oss.str(), "RMSE: 1");
}
